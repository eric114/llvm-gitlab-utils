#!/usr/bin/env bash

if [ -z "$LLVM_GITLAB_UTILS" ]; then
  echo "LLVM_GITLAB_UTILS is not set"
  exit 1
fi
source $LLVM_GITLAB_UTILS/init_shell.sh
cd $LLVM_GITLAB_UTILS

mkdir -p $SECRETS/ $OUTS/helm-home

kubectl apply -f config/gitlab-admin-service-account.yaml

kubectl get secrets/tiller-secret -n "$TILLER_NAMESPACE" -o "jsonpath={.data['ca\.crt']}" \
  | base64 --decode > $SECRETS/tiller-ca.crt
kubectl get secrets/tiller-secret -n "$TILLER_NAMESPACE" -o "jsonpath={.data['tls\.crt']}" \
  | base64 --decode > $SECRETS/tiller.crt
kubectl get secrets/tiller-secret -n "$TILLER_NAMESPACE" -o "jsonpath={.data['tls\.key']}" \
  | base64 --decode > $SECRETS/tiller.key

# Initialize helm
helm init --client-only \
    --upgrade \
    --tiller-connection-timeout 30 \

# Download the charts
helm repo add gitlab https://charts.gitlab.io

