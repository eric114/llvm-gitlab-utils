#!/usr/bin/env bash
source $LLVM_GITLAB_UTILS/init_shell.sh
set -e
set -x

cd $LLVM_GITLAB_UTILS/

kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'
MY_SECRET="$(kubectl get secrets | sed -n '2p' | cut -d " " -f 1)"
kubectl get secret $MY_SECRET -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
kubectl apply -f config/gitlab-admin-service-account.yaml
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret \
  | grep gitlab-admin | awk '{print $1}')


