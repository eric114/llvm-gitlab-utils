#!/usr/bin/env bash

if [ "${BASH_SOURCE[0]}" -ef "$0" ]
then
    echo "Hey, you should source this script, not execute it!"
    exit 1
fi

export LLVM_GITLAB_UTILS="$HOME/llvm-gitlab-utils"
if [ ! -d "$LLVM_GITLAB_UTILS" ]
then
  echo "$HOME/llvm-gitlab-utils" must exist
  exit
fi
echo Root is $LLVM_GITLAB_UTILS

export CONTEXT="$(kubectl config current-context)"
export SECRETS=$LLVM_GITLAB_UTILS/secrets/$CONTEXT
export OUTS=$LLVM_GITLAB_UTILS/out/$CONTEXT
export HELM_HOME=$OUTS/helm-home
export TILLER_NAMESPACE="gitlab-managed-apps"
export HELM_TLS_CA_CERT=$SECRETS/tiller-ca.crt
export HELM_TLS_CERT=$SECRETS/tiller.crt
export HELM_TLS_KEY=$SECRETS/tiller.key
export HELM_TLS_ENABLE=true


